import React, {useState} from 'react';
import plus from '../../assets/icons/plus.svg';
import axios from 'axios';

const AddTaskForm = ({list, onTaskAdd}) => {
    const [visibleForm, setFormVisible] = useState(false);
    const [inputValue, setinputValue] = useState('');
    const [isLoading, setisLoading] = useState(false);

    const toggleFormVisible = () => {
        setFormVisible(!visibleForm);
        setinputValue('');
    }

    const addTask = () => {
        const obj = {
            listId: list.id,
            text: inputValue,
            completed: false
        }
        setisLoading(true);
        axios.post(`http://localhost:3001/tasks`, obj).then(({data}) => {
            onTaskAdd(list.id, data)
            toggleFormVisible();
        }).catch(() => {alert('Error while processing task')}).finally(setisLoading(false))
    }
    return (
        <div className="tasks__form">
            {!visibleForm ? 
            (<div onClick={toggleFormVisible} className="tasks__form-new">
                <img src={plus} alt="Add"/>
                <span>New task</span>
            </div>)
            :
            (<div className="tasks__form-block">
            <input value={inputValue} onChange={(e) => setinputValue(e.target.value)} className="field" type="text" placeholder="Task name" />
            <button disabled={isLoading} onClick={addTask} className="button">{isLoading ? 'Adding...' : 'Add task'}</button>
            <button onClick={toggleFormVisible} className="button button--grey">Cancel</button>
            </div>)
            }
        </div>
    )
}

export default AddTaskForm;
