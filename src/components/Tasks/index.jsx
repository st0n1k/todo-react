import React from 'react'
import './tasks.scss';
import edit from '../../assets/icons/edit.svg';
import AddTaskForm from './AddTaskForm';

import axios from 'axios';

const Tasks = ({list, onEditTitle, onTaskAdd}) => {
    const editTitle = () => {
        const newTitle = prompt("What's a new title?", list.name);
        if(newTitle) {
            onEditTitle(list.id, newTitle);
            axios.patch(`http://localhost:3001/lists/${list.id}`, {name: newTitle})
            .catch(() => {alert('Can\'t update title')})
        }
    }
    return (
        <div className="tasks">
            <h2 className="tasks__title">
                {list.name}
                <img onClick={editTitle} src={edit} alt="Edit"/>
            </h2>

            <div className="tasks__items">
                {!list.tasks.length && <h2>No tasks yet</h2>}
                {list.tasks.map(task => (
                    <div key={task.id} className="tasks__items-row">
                    <div className="checkbox">
                        <input id={`task-${task.id}`} type="checkbox"/>
                        <label htmlFor={`task-${task.id}`}>
                            <svg width="18" height="14" viewBox="0 0 11 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.29999 1.20001L3.79999 6.70001L1.29999 4.20001" stroke="#000" strokeWidth="1.5" strokeLinecap="round"
                                strokeLinejoin="round"/>
                            </svg> 
                        </label>
                    </div>
                    <input readOnly value={task.text} />
                </div>
                ))}
                <AddTaskForm list={list} onTaskAdd={onTaskAdd} />
            </div>
        </div>
    )
}

export default Tasks;
