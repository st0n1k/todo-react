import React from 'react';
import './list.scss';
import classNames from 'classnames';
import Badge from '../Badge';
import axios from 'axios';
import removeBtn from '../../assets/icons/remove.svg';

const TaskBlock = ({tasks, isRemovable, onClick, onRemove, onClickItem, activeItem}) => {
    const ConfirmRemove = (item) => {
        if (window.confirm(`Do you want to remove item ${item.name}?`)) {
            axios.delete('http://localhost:3001/lists/' + item.id).then(() => {
                onRemove(item.id);
              });
        }
    }
    return (
            <ul onClick={onClick} className="list">
                {tasks.map((task, index) => (
                        <li onClick={onClickItem ? () => onClickItem(task) : null} key={index}
                        className={classNames(task.className, {active: activeItem && activeItem.id === task.id})}>
                            {task.icon ? <i>{task.icon}</i> : <Badge color={task.color.name} />}
                            <span>{task.name}
                            {task.tasks && `(${task.tasks.length})`}</span>
                            {isRemovable && <img className="list__remove-icon" onClick={() => ConfirmRemove(task)} src={removeBtn} alt="Remove icon"/>}
                        </li>
                    ))}
            </ul>
    )
}

export default TaskBlock;
