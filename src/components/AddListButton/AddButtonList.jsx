import React, { useState, useEffect } from 'react';
import TaskBlock from '../List/TaskBlock';
import Badge from '../Badge';
import axios from 'axios';
import closeBtn from '../../assets/icons/close.svg'

import './addListButton.scss';

const AddButtonList = ({colors, onAdd}) => {
    const [isOpened, setIsOpened] = useState(false);
    const [selectedColor, setSelectedColor] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        if (Array.isArray(colors)) {
          setSelectedColor(colors[0].id);
        }
      }, [colors]);
    
    const ShowForm = () => {
        setIsOpened(true);
    }
    const CloseForm = () => {
        DefaultFormValues();
    }
    const SetColor = (color) => {
        setSelectedColor(color);
    }
    const DefaultFormValues = () => {
        setIsOpened(false);
        setInputValue('');
        setSelectedColor(colors[0].id);
    }
    const addList = () => {
        if(!inputValue) {
            alert('Enter task name!');
            return;
        }
        setIsLoading(true);
        axios.post('http://localhost:3001/lists', {
            name: inputValue,
            colorId: selectedColor
          })
          .then(({ data }) => {
            const color = colors.filter(c => c.id === selectedColor)[0].name;
            const listObj = { ...data, color: { name: color } };
            onAdd(listObj);
            DefaultFormValues()
          })
          .finally(() => {
            setIsLoading(false);
          });
    }

    return (
        <div className="add-list">
            <TaskBlock onClick={ShowForm} tasks={[{icon: <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M8 1V15" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
<path d="M1 8H15" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
</svg>, name: 'Create new list', className: "list__add-button"}]} />
        {isOpened && <div className="add-list__popup">
            <img onClick={CloseForm} src={closeBtn} alt="Close button" className="add-list__popup-close-btn" />
            <input value={inputValue} onChange={(e) => setInputValue(e.target.value)} className="field" type="text" placeholder="Enter title" />
            <div className="add-list__popup-colors">
                {colors.map((color) => <Badge onClick={() => SetColor(color.id)} key={color.id} color={color.name} className={selectedColor === color.id && 'active'} />)}
            </div>
            <button onClick={addList} className="button">Submit {isLoading ? 'Adding...' : 'Submit'}</button>
        </div>}
        </div>
    )
}

export default AddButtonList;
